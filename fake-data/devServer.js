const fs = require('fs-extra');
const cors = require('cors');
const path = require('path');

const rootPath = path.resolve(__dirname);

const fixedRoutes = [
    { method: 'get', from: '/user', data: 'authOK.json' },
    { method: 'post', from: '/api/graphql', data: 'graphqlFake.json' },
    { method: 'get', from: '/api/adress', data: 'autocompleteAdr.json' },
    { method: 'get', from: '/adress', data: 'autocompleteAdr.json' },
    //{ method: 'post', from: '/oauth/token', data: 'authOK.json' },
];
function setup(app) {
    app.use(cors());
    fixedRoutes.map((route) => {
        if (route.method === 'get') {
            if (route.data && route.data.endsWith('json')) {
                getJson(app, route.from, route.data);
            }
        }
        if (route.method === 'post') {
            if (route.data && route.data.endsWith('json')) {
                postJson(app, route.from, route.data);
            }
        }
    });
}
function getJson(app, url, dataFile) {
    app.get('/api' + url, (req, res) => {
        res.json(require(`./data/${dataFile}`));
    });
}

function postJson(app, url, dataFile) {
    app.post(url, (req, res) => {
        if (dataFile) {
            res.json(require(`./data/${dataFile}`));
        } else {
            res.status(200).send('');
        }
    });
}


module.exports = setup;

import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import JwtService from '@/common/jwt.service'
import { API_URL } from '@/common/config'

const ApiService = {
  init() {
    Vue.use(VueAxios, axios)
    //Vue.axios.defaults.baseURL = API_URL
  },

  setHeader() {
    Vue.axios.defaults.headers.common['Authorization'] = `Bearer ${JwtService.getToken()}`
  },

  query(resource, params) {
    return Vue.axios
      .get(resource, params)
      .catch((error) => {
        throw new Error(`[Err] ApiService ${error}`)
      })
  },

  get(resource, slug = '') {
    return Vue.axios
      .get(`${resource}/${slug}`)
      .catch((error) => {
        throw new Error(`[Err] ApiService ${error}`)
      })
  },

  post(resource, params) {
    this.setHeader()
    return Vue.axios.post(`${resource}`, params)
  },

  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params)
  },

  put(resource, params) {
    return Vue.axios
      .put(`${resource}`, params)
  },

  delete(resource) {
    return Vue.axios
      .delete(resource)
      .catch((error) => {
        throw new Error(`[Err] ApiService ${error}`)
      })
  },

  custom(config) {
    this.setHeader()
    return Vue.axios(config)
  }
}

export default ApiService

export const AuthService = {
  authenticate(mail, password) {
    const bodyFormData = new FormData()
    bodyFormData.set('grant_type', 'password')
    bodyFormData.set('scope', 'webclient')
    bodyFormData.set('username', mail)
    bodyFormData.set('password', password)

    return Vue.axios({
      method: 'post',
      url: 'http://172.31.52.152:8082/oauth/token',
      auth: {
        username: 'nextgenui',
        password: 'richard'
      },
      data: bodyFormData,
      config: { headers: { 'Content-Type': 'multipart/form-data' } }
    })

  }
}

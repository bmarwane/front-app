import axios from 'axios'


const state = {
    activated: false,
    activating: false,
    adressesResult: [],
    chosenAdress: null
}

const getters = {
    adressesResult(state) {
        return state.adressesResult
    }
}

const actions = {

    enableAutocomplete(context) {
        const { activating, activated } = context.state
        if(activating || activated) return;
        context.commit('setActivating', !context.state.activating)

        setTimeout(() => {
            context.commit('setActive')
        }, 800)
    },

    fetchAdresses(context, searchStr) {
        if(context.state.activated) {
            axios.get('https://api-adresse.data.gouv.fr/search/?q=' + searchStr)
            //axios.get('/api/adress')
            .then((response) => {
                context.commit('setAdressesResult', response.data.features.map(r => r.properties))
            })
        } else {
            context.dispatch('enableAutocomplete')
        }
    },

    chooseAdress(context, chosenAdress) {
        context.commit('setChosenAdress', chosenAdress)
        context.commit('setAdressesResult', null)
    }
}

const mutations = {
    setAdressesResult(state, result) {
        state.adressesResult = result
    },
    setActivating(state, value) {
        state.activating = value
    },
    setActive(state) {
        state.activated = true
    },
    setChosenAdress(state, chosenAdress) {
        state.chosenAdress = chosenAdress
    }
}

export default {
    state,
    actions,
    mutations,
    getters
}

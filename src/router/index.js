import Vue from 'vue'
import Router from 'vue-router'
import store from 'store'


Vue.use(Router)



export default new Router({
    mode: 'history',
  routes: [
      {
          name: 'home',
          path: '/',
          component: () => import('views/NotSecured'),
      },
      {
          name: 'login',
          path: '/login',
          component: () => import('views/Login')
      },
      {
          name: 'secured',
          path: '/secured',
          component: () => import('views/Secured'),
          beforeEnter: ifAuthenticated
      },
  ]
})


function ifAuthenticated(to, from, next) {
    if (store.getters.isAuthenticated) {
        next()
        return
    }
    next('/login')
}

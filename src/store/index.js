import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth.module'
import secure from './secure.module'
import form from 'components/Form/form.module'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    secure,
    form
  }
})

import ApiService from '@/common/api.service'

const state = {
    data: null
}

const getters = {
    gqlData: (state) => {
        return state.data
    },
}

const actions = {
    loadDataFromApi(context) {
        return ApiService.custom({
            url: 'http://localhost:4000/graphql',
            method: 'post',
            data: {
                query: `{hello}`
            }
        }).then(data => context.commit('setGqlData', data))
    }
}

const mutations = {
    setGqlData(state, data) {
        state.data = data
    }
}

export default {
    state,
    actions,
    mutations,
    getters
}
